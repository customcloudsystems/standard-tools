<?php

namespace CustomCloudSystems\Traits;

/**
 * Trait Settable
 * @package CustomCloudSystems\Traits
 * @author Custom Cloud Systems LLC
 */
trait Settable
{
    public function __set($name, $value)
    {
        if ( method_exists( get_class( $this ), "set_{$name}" ) ) {
            $method = "set_{$name}";
            $this->$method($value);
        }

        if ( property_exists( get_class( $this ), $name ) ) {
            $this->{$name} = $value;
        }
    }
}
