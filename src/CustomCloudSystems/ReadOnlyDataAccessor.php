<?php

namespace CustomCloudSystems;

/**
 * Class ReadOnlyDataAccessor
 * @package CustomCloudSystems
 * @author Custom Cloud Systems
 */
class ReadOnlyDataAccessor extends DataAccessor
{
    /**
     * Prevents writes to this instance of data store
     * @param $key
     * @param $value
     * @throws \Exception
     */
    public function set($key, $value)
    {
        throw new \Exception('Cannot write to this data store');
    }
}