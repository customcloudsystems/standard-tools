<?php


namespace CustomCloudSystems\Traits;

/**
 * Trait ArrayModifier
 * @package CustomCloudSystems\Traits
 * @author Custom Cloud Systems LLC
 */
trait ArrayModifier
{
    // Remove null values from an array recursively
    //
    public function recursiveStrip(&$data)
    {
        foreach ($data as $key => $val)
        {
            if (is_null($data[$key]) && array_key_exists($key, $data)) {
                unset($data[$key]);
            } else if (is_array($data[$key])) {
                $this->recursiveStrip($data[$key]);
            }
        }
    }
}