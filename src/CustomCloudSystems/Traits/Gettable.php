<?php

namespace CustomCloudSystems\Traits;

/**
 * Trait Gettable
 * @package CustomCloudSystems\Traits
 * @author Custom Cloud Systems LLC
 */
trait Gettable
{
    public function __get( $name )
    {
        if ( method_exists( get_class( $this ), "get_{$name}" ) ) {
            $method = "get_{$name}";
            return $this->$method();
        }

        if ( property_exists( get_class( $this ), $name ) ) {
            return $this->{$name};
        }

        return null;
    }
}