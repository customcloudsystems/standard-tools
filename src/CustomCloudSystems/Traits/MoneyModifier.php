<?php


namespace CustomCloudSystems\Traits;

/**
 * Trait MoneyModifier
 * @package CustomCloudSystems\Traits
 * @author Custom Cloud Systems LLC
 */
trait MoneyModifier
{
    /**
     * @param $value
     * @return float
     */
    protected function stripDollarSignAndCast($value)
    {
        return (float) str_replace('$', '', $value);
    }
}