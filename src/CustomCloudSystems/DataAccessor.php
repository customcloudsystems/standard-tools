<?php

namespace CustomCloudSystems;

/**
 * Class DataAccessor
 * @package CustomCloudSystems
 * @author Custom Cloud Systems
 */
class DataAccessor
{
    /**
     * Data store
     * @var array $data
     */
    private $data;

    /**
     * DataAccessor constructor.
     * @param array $data
     */
    public function __construct($data = [])
    {
        $this->data = $data;
    }

    /**
     * Set value to store
     * @param $key
     * @param $value
     */
    public function set($key, $value)
    {
        $this->data[$key] = $value;
    }

    /**
     * Get value from store
     * @param $key
     * @param string $default
     * @return mixed|string
     */
    public function get($key, $default = '')
    {
        if (array_key_exists($key, $this->data)) {
            return $this->data[$key];
        }

        return $default;
    }

    /**
     * Check if a key exists
     * @param $key
     * @return bool
     */
    public function exists($key)
    {
        return array_key_exists($key, $this->data);
    }

    /**
     * Throw an exception of a key does not exist
     * @param $key
     * @throws \Exception
     */
    public function existsThrowError($key)
    {
        if (! $this->exists($key)) {
            throw new \Exception("Key {$key} does not exist in data store");
        }
    }

    /**
     * Get the first value in the array and return as DataAccessor
     * @return DataAccessor
     */
    public function first()
    {
        if (isset($this->data[0])) {
            return new self($this->data[0]);
        }

        return new self;
    }
}
