<?php


namespace CustomCloudSystems\Traits;

/**
 * Trait SourceLoader
 * @package CustomCloudSystems\Traits
 * @author Custom Cloud Systems LLC
 */
trait SourceLoader
{
    public function loadArray( $data = [] )
    {
        if ( isset( $data ) && is_array( $data ) ) {
            foreach ( $data as $key => $value ) {
                if ( property_exists($this, $key) ) {
                    $this->{$key} = $value;
                }
            }
        }
    }
}