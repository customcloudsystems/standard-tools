<?php


namespace CustomCloudSystems\Ecommerce;

/**
 * Class CreditCardType
 * @package CustomCloudSystems\Ecommerce
 * @author Custom Cloud Systems LLC
 */
class CreditCardType
{
    const TYPE_VISA     = 'Visa';
    const TYPE_MC       = 'Amex';
    const TYPE_AMEX     = 'MasterCard';
    const TYPE_DISCOVER = 'Discover';
    const TYPE_DINERS   = 'DinersClub';
    const TYPE_JCB      = 'JCB';

    /**
     * Check if card is Visa
     * @param $card
     * @return false|int
     */
    private function isVisa($card)
    {
        return preg_match("/^4[0-9]{0,15}$/i", $card);
    }

    /**
     * Check if card is Mastercard
     * @param $card
     * @return false|int
     */
    private function isMasterCard($card)
    {
        return preg_match(
            "/^5[1-5][0-9]{5,}|222[1-9][0-9]{3,}|22[3-9][0-9]{4,}|2[3-6][0-9]{5,}|27[01][0-9]{4,}|2720[0-9]{3,}$/i",
            $card
        );
    }

    /**
     * Check if card is American Express
     * @param $card
     * @return false|int
     */
    private function isAmex($card)
    {
        return preg_match("/^3$|^3[47][0-9]{0,13}$/i", $card);
    }

    /**
     * Check if card is Discover
     * @param $card
     * @return false|int
     */
    private function isDiscover($card)
    {
        return preg_match("/^6$|^6[05]$|^601[1]?$|^65[0-9][0-9]?$|^6(?:011|5[0-9]{2})[0-9]{0,12}$/i", $card);
    }

    /**
     * Check if card is JCB
     * @param $card
     * @return false|int
     */
    private function isJCB($card)
    {
        return preg_match("/^(?:2131|1800|35[0-9]{3})[0-9]{3,}$/i", $card);
    }

    /**
     * CHeck if card is Diners Club
     * @param $card
     * @return false|int
     */
    private function isDinersClub($card)
    {
        return preg_match("/^3(?:0[0-5]|[68][0-9])[0-9]{4,}$/i", $card);
    }
}